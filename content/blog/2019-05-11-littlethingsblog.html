---
title: Some things I learned when working with functions in R
author: Katrin Tirok
date: '2019-05-11'
slug: littlethingsblog
categories: ["R"]
tags: ["R", "tidyverse", "data science"]
---



<p>When working on a data analysis or visualisation with <em>R</em>, there are always some little things that we discover and that are often useful in other projects too. Here, I collected a few of such little things that where new to me at the time, but helpful to remember, especially when writing functions in <em>R</em>.</p>
<div id="loading-packages" class="section level2">
<h2>1. Loading packages</h2>
<p>Packages can be loaded with <code>library()</code> or with <code>require()</code>. In many cases either way is fine. <code>library()</code> will throw an error and stop when a package is not installed, whereas <code>require()</code> will give a warning and continue. <code>require()</code> also returns a logical value and thus can be built into conditional statements. Functions of installed packages can also be used by calling the name of the package with <code>::</code> and then the name of the function without specifically loading the package, e.g. <code>dplyr::select(mydata,var1)</code>. When using different packages which contain functions with the same name, they will mask each other (‘namespace collisions’). In that case using <code>::</code> is essential to point R and other users of the script to the correct package.</p>
</div>
<div id="extracting-variables-from-dataframes" class="section level2">
<h2>2. Extracting variables from dataframes</h2>
<p>One of the first things we learn when working with <em>R</em> is using <code>$</code> or <code>[]</code> to choose a column from a dataframe (or tibble). When checking on the result with <code>class()</code> we can quickly see that they do slightly different things. <code>$</code> returns a vector, whereas <code>[]</code> returns a dataframe.</p>
<p>Let’s create some sample data to play with. I generate some arbitrary temperature data over a day in a dataframe.</p>
<pre class="r"><code>mydata &lt;- data.frame(time = c(6, 8, 10, 12, 14, 16, 18, 20),
                     temp = c(10, 14, 18, 22, 24, 24, 22, 20))
mydata</code></pre>
<pre><code>##   time temp
## 1    6   10
## 2    8   14
## 3   10   18
## 4   12   22
## 5   14   24
## 6   16   24
## 7   18   22
## 8   20   20</code></pre>
<p>Now, extracting the column <code>time</code> from the dataframe <code>mydata</code> using <code>$</code> returns a numeric vector:</p>
<pre class="r"><code>mydata$time</code></pre>
<pre><code>## [1]  6  8 10 12 14 16 18 20</code></pre>
<pre class="r"><code>class(mydata$time)</code></pre>
<pre><code>## [1] &quot;numeric&quot;</code></pre>
<p>and using <code>[]</code> returns a <code>data.frame</code>:</p>
<pre class="r"><code>mydata[&#39;time&#39;]</code></pre>
<pre><code>##   time
## 1    6
## 2    8
## 3   10
## 4   12
## 5   14
## 6   16
## 7   18
## 8   20</code></pre>
<pre class="r"><code>class(mydata[&#39;time&#39;])</code></pre>
<pre><code>## [1] &quot;data.frame&quot;</code></pre>
<p>When working with <code>dplyr</code> we can use <code>select()</code>, which also returns a dataframe:</p>
<pre class="r"><code>dplyr::select(mydata, time)</code></pre>
<pre><code>##   time
## 1    6
## 2    8
## 3   10
## 4   12
## 5   14
## 6   16
## 7   18
## 8   20</code></pre>
<pre class="r"><code>class(dplyr::select(mydata, time))</code></pre>
<pre><code>## [1] &quot;data.frame&quot;</code></pre>
<p>For many things we do with the extracted data, it does not matter whether we have the data in form of a vector or in form of a dataframe, but sometimes the class becomes important and then we should be aware of the difference.</p>
<p>In some cases we want to subset a dataframe using another variable, which contains the column name, for example when working with functions. In this case we cannot use <code>$</code> to extract the chosen column as a vector. We can use instead <code>[[]]</code> to extract columns from dataframes and get the result as a vector.</p>
<pre class="r"><code>myvar = &#39;time&#39;
mydata[[myvar]]</code></pre>
<pre><code>## [1]  6  8 10 12 14 16 18 20</code></pre>
<pre class="r"><code>class(mydata[[myvar]])</code></pre>
<pre><code>## [1] &quot;numeric&quot;</code></pre>
<p>All the methods described above work the same way with <code>data.frame</code> and <code>tibble</code>.</p>
</div>
<div id="using-alternative-versions-of-functions-when-programming-with-column-names-hold-as-values-in-variables" class="section level2">
<h2>3. Using alternative versions of functions when programming with column names hold as values in variables</h2>
<p>Functions from <code>dplyr</code>, <code>ggplot2</code> and other packages allow us to pass the name of a dataframe via the <code>data</code> argument and then directly pass the plain variable names to a function without having to use quotation marks or having to refer back to the dataframe, e.g.</p>
<pre class="r"><code>library(ggplot2)
ggplot(data = mydata, aes(x = time, y = temp)) + 
  geom_point()</code></pre>
<p><img src="/blog/2019-05-11-littlethingsblog_files/figure-html/unnamed-chunk-5-1.png" width="288" style="display: block; margin: auto;" /></p>
<p>versus base plot, e.g. <code>plot(x = mydata$time, y = mydata$temp)</code>.</p>
<p>This makes coding faster, but does not work with column names that are hold as values in variables, as often the case when writing functions, loops etc.</p>
<p>Here is an example, the generated plot doesn’t show our data correctly:</p>
<pre class="r"><code>myvar = &#39;temp&#39;
ggplot(data = mydata, aes(x = time, y = myvar)) +
  geom_point()</code></pre>
<p><img src="/blog/2019-05-11-littlethingsblog_files/figure-html/unnamed-chunk-6-1.png" width="288" style="display: block; margin: auto;" /></p>
<p>For these cases, many functions provide alternative versions. With <code>ggplot()</code> we can use <code>aes_string()</code> to pass column names in string notation, i.e. in qoutation marks (note, all column names need to be in quotation marks in this case):</p>
<pre class="r"><code>myvar = &#39;temp&#39;
ggplot(data = mydata, aes_string(x = &#39;time&#39;, y = myvar)) + 
  geom_point()</code></pre>
<p><img src="/blog/2019-05-11-littlethingsblog_files/figure-html/unnamed-chunk-7-1.png" width="288" style="display: block; margin: auto;" /></p>
<p>With <code>dplyr</code> we can use the group of <code>_</code> functions, which evaluate the content of a variable, e.g. <code>group_by_()</code>.</p>
<p>Let’s generate a third column in our dataframe, which can be used to group values:</p>
<pre class="r"><code>library(dplyr)
mydata$var3 &lt;- c(&#39;morning&#39;,&#39;morning&#39;, &#39;morning&#39;, &#39;afternoon&#39;, &#39;afternoon&#39;,
                 &quot;afternoon&quot;, &#39;evening&#39;, &#39;evening&#39;)
group_var = &#39;var3&#39;</code></pre>
<p>Now let’s group and summarise our data using <code>group_by()</code> (does not work) vs. <code>group_by_()</code> (works):</p>
<pre class="r"><code>mydata %&gt;% 
  group_by(group_var) %&gt;% 
  summarise(temp_av = mean(temp))</code></pre>
<pre><code>## Error: Column `group_var` is unknown</code></pre>
<pre class="r"><code>mydata %&gt;% 
  group_by_(group_var) %&gt;% 
  summarise(temp_av = mean(temp))</code></pre>
<pre><code>## # A tibble: 3 x 2
##   var3      temp_av
##   &lt;chr&gt;       &lt;dbl&gt;
## 1 afternoon    23.3
## 2 evening      21  
## 3 morning      14</code></pre>
</div>
